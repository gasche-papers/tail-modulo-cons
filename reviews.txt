> SUBMISSION: 22
> TITLE: Tail Modulo Cons
>
>
> ----------------------- REVIEW 1 ---------------------
> SUBMISSION: 22
> TITLE: Tail Modulo Cons
> AUTHORS: Bour Frédéric, Basile Clément and Gabriel Scherer
>
> ----------- Overall evaluation -----------
> SCORE: 2 (accept)
> ----- TEXT:
> Summary
> ╌╌╌╌╌╌╌
>
>   This article presents a new optimization in OCaml, called Tail Modulo
>   Cons (TMC). When switched on for the definition of a recursive
>   function, this optimization recognizes that recursive calls are in a
>   shape such that they can be rewritten as tail recursive in an
>   intermediate representation of OCaml compiled expressions (called
>   Lambda), and performs the corresponding transformation. The required
>   shape (informally, recursive calls must be under constructor
>   applications only) is restrictive but it allows the transformation to
>   be simple enough to avoid a loss in performance.
>
>
> Overall evaluation
> ╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌
>
>   The article (except Sec.4) is well illustrated by examples to
>   understand the transformation, its design and limitations. Lots of
>   interesting details show the will to make it as beneficial for users
>   as possible. In addition to presenting the optimization, this articles
>   make the reader enter smoothly in (one aspect of) the OCaml compiler.
>
>
> Detailed comments
> ╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌
>
>   I appreciated a lot the examples of the paper, and I was disappointed
>   that there is no example in Sec.4, which makes it much harder to read.
>   For instance, it would be useful to have an example of the various
>   transformations applied step by step.
>
>   In the prologue, the example of the evaluation order seems quite
>   orthogonal to what you present in the paper, in particular since, as
>   you say, your optimization changes the evaluation order: if one wants
>   guaranties on the evaluation order, s/he still has to use a `let`
>   binding.
>
>   What is `Tuple` in Sec.2?

Ajouté : ($\kwd{Tuple}$ is an arbitrary constructor name used to inject tuples
into the grammar) NB: On devrait de toute façon expliciter que les tuples sont
utilisés pour faire des multi-arguments; voir aussi remarque du reviewer 2.
-- Basile

J'ai reformulé un poil ce paragraphe, mais on peut revert si ça ne te va pas.
-- Gabriel

Ta reformulation me parait bien. -- Basile

>
>   You require to switch explicitly this optimization on for this
>   optimization, by using the syntax [@tail_mod_cons]. One argument for
>   it is the scope of keeping the transformed sub-function, as explained
>   in Sec.5.5. Another approach would try to perform this optimization
>   whenever possible. Could you comment on the advantages and drawbacks
>   of such a solution?
>
>   p.2: I do not understand "(or maybe, if we are teaching an advanced
>   class, after the second version)". I would expect a second version of
>   something being more accessible to non-advanced users… Or is it
>   because a second version would get rid of [@tail_mod_cons], as I
>   suggest above?
>
>   In Fig.2 (resp. 3), `e;T` (resp. `e;U`) is a subcase of `let x = e in
>   T` (resp. `let x = e in U`), I think you can remove it.
>
>   The syntax of the first rule for the DPS transformation seems
>   incorrect. If it is correct, you should explain it.
>
>   Typos:
Corrigées. -- Basile
>   • p.4: "FIbonacci"
>   • p.5: "arithematic"
>   • p.6: "each hole indicate"
J'ai changé en: "each hole indicates"; je crois que c'est correct? -- Basile
>   • p.6: "a call `f e` is in tail position […] in expression `e'` if
>     there is a decomposition of `e` as […]." My understanding is that it
>     should be a decomposition of `e'`.
>   • p.15: "{]"
>   • p.15: "Another alternatives"
>   • p.17: "[version"
>
>
>
> ----------------------- REVIEW 2 ---------------------
> SUBMISSION: 22
> TITLE: Tail Modulo Cons
> AUTHORS: Bour Frédéric, Basile Clément and Gabriel Scherer
>
> ----------- Overall evaluation -----------
> SCORE: 2 (accept)
> ----- TEXT:
> * résumé
>
> Les fonctions récursives faisant des appels non-terminaux sont souvent problématiques en OCaml, car elles consomment rapidement l'espace disponible sur la pile et ne peuvent donc pas être utilisées sur des arguments de taille pourtant raisonnable.
>
> Ce problème force de nombreux programmeurs à repenser/compliquer leur code afin de rendre les appels critiques récursif terminaux. Outre la clarté et la correction, cela affecte aussi les performances, une fonction comme List.map étant plus lente lorsqu'elle est écrite en mode récursif terminal.
> Ce problème est bien connu et a poussé les concepteurs de plusieurs librairies à définir des fonctions de base comme List.map de façon très compliquée et en utilisant des fonctionnalités non sures pour contourner la difficulté.
>
> Cet article présente une optimisation pour le compilateur OCaml, permettant de transformer automatiquement certains appels récursifs non-terminaux en appels récursifs terminaux, afin de limiter l'usage de la pile et tout en conservant l'efficacité.
> C'est une transformation de programme qui ne peut pas être effectuée au niveau du langage source; les auteurs l'implémentent donc à un niveau intermédiaire (Lambda), où l'on peut travailler sur des valeurs partiellement initialisées, et mutables.
>
> L'interface utilisateur prend la forme d'indications dans le code source, indiquant quelles fonctions doivent être transformées, et au besoin quels appels doivent être privilégiés (dans certains cas, il n'est pas possible de rendre tous les appels récursifs terminaux)
>
> La transformation fonctionne pour les appels tail récursifs terminaux 'modulo constructeurs', c'est à dire où la seule chose restant à faire après l'appel est l'ajout de constructeurs connus à l'avance autour de la valeur retournée par l'appel. Dans ce cas, le constructeur peut être créé avant l'appel, avec un trou, de sorte que l'on puisse faire un appel (terminal) à une fonction transformée qui se contente de boucher le trou. C'est une transformation 'destination passing style', plus restreinte que les CPS, mais qui permet d'être plus efficace.
>
> Les auteurs montrent en appendice que leur transformation permet avec un tout petit peu de déroulage d'obtenir les performances des meilleures librairies pour List.map, tout en gardant un code source clair et lisible.
>
>
>
> * jugement
>
> Ce travail est très intéressant, et sera certainement très utile à l'écosystème OCaml.
> Il exploite des idées relativement anciennes (1973, 1975, 1998), qui n'avaient manifestement pas encore été exploitées au sein d'OCaml.
> L'article est bien motivé, et bien écrit en grande partie.
>
> Il pêche un peu dans la section 4, où une description semi-formelle de la transformation est donnée.
> Il y a au moins une petite erreur (la syntaxe des contextes n'est pas assez permissive), et il me semble que le formalisme pourrait être pas mal simplifié (cf. détails plus bas).
>
> Les auteurs assument comme 'non-contribution' de ne pas prouver la correction de leur transformation.
> Cette question est assez critique, mais j'imagine bien épineuse, et je comprends qu'elle ne soit pas traitée ici.
>
> Dans la mesure où les JFLAs acceptent des travaux 'en cours', il me semble que cet article mérite tout à fait d'y être présenté.
>
> J'espère néanmoins que les auteurs trouveront du temps et des outils pour justifier à terme la correction de cette transformation.
>
>
> J'ai trouvé l'appendice très intéressant ; peut-être faut-il trouver un moyen de le conserver dans la version finale (4.5 pages)
>
>
>
> * remarques
>
> p4, 1.3.1
> - je n'ai pas compris l'item 2 du premier coup. en première lecture, je trouvais que c'était plutôt un argument pour implémenter CPS...
>   rajouter 'the CPS can *already* be implemented at the source level *by end-users*' et rajouter 'so that it is preferable not to leave it to end users' à la fin aiderait peut-être à comprendre directement.
> - le troisième item n'est pas vraiment un argument pour préférer TMC:
>   . la première partie est un choix de design (intéressant)
>   . la deuxième partie est un choix subjectif
>   -> je sortirais de l'itemize après le 2nd item, puis je dirais "On fait le choix de faire opt-in ce qui permet blablabla. Whether a similarly strategy can be followed for CPS remains for future work."
>
> p4 fin de 1.3.2 que sont les 'strict functions' ?
>
>
> p6 Fig 1.
> Mentionner qqpart dans le texte que vous vous concentrez sur les fonctions unaires, et que les constructeurs de tuples permettent de faire le cas général.
> Quid de l'implem?

C'est également ce que fait l'implèm (on ne gère pas la currification).  On en
a parlé avec Gabriel mais finalement pas mentionné dans le papier… Oups.
-- Basile

Pour moi l'implémentation marche très bien avec des fonctions n-aires
(heureusement !), donc on n'est pas restreint aux fonctions unaires
(et on n'a pas besoin de tout tuplifier). On ne gère pas l'ordre
supérieur, mais des définitions de la forme "let f x = fun y -> fun
z -> ..." (fonctions anonymes immédiates) sont normalisées en
fonctions en-pratique-ternaires avant Lambda.
-- Gabriel

Oui je me suis emmêlé les pinceaux; j'ai quotienté par l'équivalence fonction
n-aires / fonctions avec un tuple immédiatement déconstruit en argument.  J'ai
rajouté deux phrases pour mentionner cela.
-- Basile

> p6 "but it cannot specialize higher-order function arguments for TMC"
> on comprend avec cette phrase qu'il y a un soucis avec l'ordre supérieur, mais ce n'est pas clair
> -> à reformuler et détailler un peu.
>
>
> p7 fig2 et p8 fig 3
> - bizarre d'inclure la construction let rec dans les deux cas, puisque vous avez annoncé à la page d'avant supposer que les let-rec sont donnés de façon globale, à top-level (d'où l'entrée séparé stmt dans la fig. 1 -- qui n'est pas vraiment utilisée dans le texte, d'ailleurs)

C'est vrai… d'ailleurs on n'utilise jamais `let rec` ensuite dans les
transformations.  Je crois que c'était pour expliquer le "scope" de la
transformation qu'il était là, mais c'est fait dans une autre section (et est
assez orthogonal en fait).  Je propose de simplement enlever les `let rec` des
grammaires pour T et U (et peut-être mentionner les let-rec internes dans la
section 5.5 ?). -- Basile

Ça me va. On a fait le choix de pas/peu parler du driver dans ce travail.
-- Gabriel

J'ai enlevé les `let rec` des grammaires T et U.
-- Basile

>
> - plus important: en l'état, le contexte
>   match e with [] -> e' | x::q -> \box
>   n'existe pas (e' est une expression sans trou)
>   n'est-ce pas un problème ?

Non, cela rendrait la transformation incorrecte !  De fait, si je me souviens
bien, l'implémentation faisait cette erreur à un moment.  Le problème survient
quand on transforme:

`d.n <- match e with [] -> e' | x :: q -> \box`

qu'il ne faut surtout pas transformer en

`match e with [] -> e' | x :: q -> d.n <- \box`

Bon, en écrivant cela, je me rends compte que c'est un peu artificiel — on
pourrait simplifier en ajoutant un cas `e` aux contextes U et T, et en
rajoutant la règle :

`d.n <- e ->_dps d.n <- e`

Ça éviterait de devoir capturer un `U[e_1, ..., e_n]` au profit de plutôt un
`U[f_1 e1, ..., f_n e_n]` avec uniquement des `f_i` qui a une version DPS.  On
ne voit plus bien que les appels de fonction non-DPS ne sont plus en tail
position (c'est caché dans cette nouvelle règle)… mais on ne le voit pas très
bien dans la présentation actuelle non plus.  J'y réfléchis. -- Basile


De mon côté j'aime bien la position de maximalité de la présentation
multi-trou actuelle: que ce soit pour les grammaires T ou U, *toutes
les positions tail* dans le terme décomposé se situent dans un des
trous. On perd cette propriété avec la proposition.
-- Gabriel

Une autre mauvaise propriété de cette proposition est qu'elle créé une
ambiguité dans la grammaire, où un même U peut être vu comme un
contexte de plusieurs façons différentes. Par exemple le contexte
((a; b), □) peut être décomposé de deux façons, une avec (a;b) comme
feuille (donc la transformation DPS donnerait (d.n <- a;b)), et une
autre avec seulement b comme feuille (donc (d.n <- b)). Le fait que le
résultat dépende de la façon de "parser" U comme contexte me semble
problématique.
-- Gabriel

Je suis d'accord avec ce point.  Plutôt pour garder une approche dans la veine
de la présentation actuelle, du coup.
-- Basile

>
>
> p9 deux derniers items au début de section 4
>   je n'ai pas réussi à suivre, les explications ne sont pas assez précises.
>   j'ai d'abord été perdu par la notation x.y<-U, qui n'est pas un contexte mais la paire de deux expressions(variables?) et d'un contexte, notée de façon suggestive pour faciliter la suite
>   puis par la notation T[x1.y1<-\box ...] qui n'est à nouveau pas un contexte, mais un objet hybride.
>   -> à reprendre
>   (j'ai à nouveau bloqué sur la notation \box[let f=e in e'] au milieu de p4, puis compris seulement en fin de p4)
>   mais je propose une amélioration de la présentation formelle; à voir plus bas

En fait `x.y <- U` et `T[x1.y1 <- \box ...]` sont bien des contextes… dont la
grammaire générale est implicite n'est pas définie (c'est celle de `Exprs` où
tous les `e` sont des contextes).  On doit pouvoir être plus clair (je voulais
faire la présentation à base de notation de tuples initialement, puis j'ai
changé, je ne sais plus bien pourquoi) -- Basile

>
>
> p10 "which is incorrect, as we explain"
>   le mot 'incorrect' est trop fort je trouve, j'ai cru qu'il voulait dire 'complètement faux' alors qu'on découvre en section 4.2 qu'il voulait dire 'sous-optimal, certains appels tail-récursifs ne le sont plus après transformation'.

Changé incorrect en suboptimal. -- Basile

>
>
> p10 est-ce que la présentation de la transformation ~~>_dps
>     ne pourrait pas être simplifiée comme suit:
>
>     utiliser une présentation des contextes avec des trous numérotés \box_i,
>     puis, partant de [let rec f x = e] avec f à transformer,
>     décomposer e en U[f1 e1,...], fixer les fi ei,
>     puis utiliser une relation
>
>     d.n<-U ~~>_dps e'
>
>     (où d.n<-U est une notation pour un tuple d,n,U et e' est juste une expression)
>     telle qu'on puisse définir [let rec f_dps d n x = e']
>
>     le cas de base devient
>     d.n<-\box_i ~~>_dps d.n<-fi ei   ou   fi_dps d n ei   selon le statut de fi
>
>     tous les autres cas abbrègent leur T[di.ni<-\box] en un e'
>
>     si ça marche effectivement, je trouve cette présentation plus naturelle, et il me semble qu'elle permet de mieux comprendre pourquoi il faut garder tous les appels TMC, pas seulement ceux qu'on va pouvoir optimiser
>
>
>     idem pour la transformation avec compression

Essentiellement ceci est similaire à ce que je dis plus haut : l'idée
principale est de capturer des trous *uniquement* au niveau des appels de
fonction.  Il y a trois approches "naturelles" de la décomposition:

 1 Capturer des trous partout (y compris lorsqu'il n'y a pas d'appel de
   fonction), c'est-à-dire que chaque branche d'exécution (dans un match par
   exemple) contient au moins un trou.  C'est la présentation que l'on a
   choisie, qui capture toutes les *tail positions* (resp. modulo con), et
   possède la symmétrie avec les contextes d'évaluation.
   Exemple: `match e1 with [] -> e2 | x :: q -> f e3` se décompose:
   `(match e1 with [] -> \hole | x :: q -> \hole)[e2, f e3]`

 2 Capturer des trous uniquement lors d'un appel de fonction, i.e. on capture
   uniquement les *tail calls* (resp. modulo con).  Il n'y a pas de trous s'il
   n'y a pas d'appel de fonction.  C'est la présentation suggérée par le reviewer.
   Exemple: `match e1 with [] -> e2 | x :: q -> f e3` se décompose:
   `(match e1 with [] -> e2 | x :: q -> \hole)[f e3]`

 3 Capturer des trous uniquement lors d'un appel de fonction transformable en
   DPS.

J'ai utilisé 1 dans le but de mieux découpler la présentation, en pratique ça a
l'air de ne pas avoir été bien compris…  En pratique c'est aussi je crois ce
qui est le plus proche de la façon dont on traite le champ `Choice.dps` dans
l'implémentation.  Utiliser 2 permet de mieux voir pourquoi on doit garder les
appels non-DPS; mais c'est un peu artificiel — de toute façon, pour un terme
comme `let x = e in f e'` où `f` n'a pas de version DPS, on ne veut pas
`(let x = e in \hole)[f e].  Je veux bien vos avis. -- Basile

Je suis pour expliquer clairement que nous avons fait le choix de la
maximalité, et garder la présentation actuelle, parce que c'est le
plus facile.
-- Gabriel

Agreed; je valide tes ajouts en ce sens.
-- Basile

>
>
>
> p10 last rule for the transformation (and elsewhere):
> why using a let for d'?
> doing d.n <- K(...) is equivalent and slightly easier to parse, no?
> (it could matter if d and n where not variables, but they are, aren't they?)

On utilise un `let` pour `d'` parcequ'on utilise aussi `d'` dans la prémisse.
-- Basile

C'est un peu subtil et il serait bien de le dire explicitement dans le texte, si ce n'est pas déjà fait.
-- Gabriel

Tu l'as fait (je laisse une note ici pour la postérité).
-- Basile

>
>
> p11 "Finally, for each ei with shape fi e'i where fi has a DPS version, we replace ..."
>   J'ai réalisé ici que ça pouvait être anticipé lors du calcul de la décomposition de e en U[...],
>   ce qui peut permettre de limiter les situations où l'utilisateur doit déclarer quels appels optimiser
>     (e.g.,
>     let rec f[@tmc] x = K(f ., g .) and g x = ...
>     où les appels à f et g sont tous les deux TMC, mais seul le premier peut-être optimisé puisqu'on ne demande pas la transformation de g)
>   J'imagine que l'implémentation marche déjà comme ça, mais il faut peut-être insister sur ce point lors des explications sur les insertions de [@tailcall]

J'ai un doute sur le comportement de l'implémentation dans ce cas — est-ce que
l'on aura un warning ? Gabriel ? -- Basile

Ici il n'y a pas de warning, l'appel à g donne un Choice.return et il n'est pas en compétition avec l'appel à f.
P.S.: J'ai le sentiment que le reviewer intuite une présentation plus proche de l'approche "tuplée"
dans ma propre présentation, quand il dit "anticipé lors du calcul de la décomposition", ça revient
à faire en une passe des feuilles vers la racine.
-- Gabriel

OK; est-ce que tu veux rajouter un truc là-dessus dans la présentation ?
-- Basile

> p11 explications juste avant 4.2 : l'exemple concret serait bienvenu
>
> p12 pourquoi ne pas inliner le let dst' ?

Pareil que plus haut; on n'inline pas le `let dst'` car on utilise deux fois
`dst'`. -- Basile

>
> p14 "Reviewers have found this to be an issue"
>  des reviewers du code/feature ou d'une version précédente de l'article
>  dans les deux cas, je trouve un peu étrange de formuler la proposition ainsi.
>
> p15 transformation de flatten en flatten+append_flatten
> y-a t'il un espoir d'automatiser cette transformation?
> c'est très joli, mais j'aimerais ne jamais avoir à le faire moi-même!
>
>
> p16 5.6 higher-order transformation
> pourquoi pas, mais ne risque t'on pas de perdre totalement le contrôle sur la linéarité nécessaire à la correction de la transformation?
>
> p17 "(All measurements used OCaml 4.10)"
> c'est une info générale, non spécifique à cet item
> -> remonter l'info, ou préciser dans l'item qu'il s'agit de la stdib de OCaml 4.10
>
> p17 pourquoi dérouler 5x d'un côté et 4x de l'autre?
>
>
>
>
> * typos

Corrigées. -- Basile

>
> p2 "let rec map_ acc = ... map*_* (f x::acc)"
>
> p4 "monadic-bindg"
>
> p4 "FIbonacci"
>
> p4, footnote 1: direct-style -> destination-passing style ?

Je suppose que oui, mais je veux bien confirmation de Gabriel. -- Basile

J'ai clarifié. -- Gabriel

>
> p5 "List.append li \box; we only support": reprendre la transition au niveau du point virgule

J'avoue ne pas comprendre la phrase non plus… -- Basile

Je voulais dire:
- en prolog, on peut représenter (List.append li \box) comme une difference-list
  (List.append li X, X)
- mais nous on ne sait pas faire ça, on ne peut parler que de contextes qui sont formés
  d'applications directes de constructeurs.
Je vais essayer de clarifier.
-- Gabriel

C'est plus clair comme ça merci !
-- Basile

>
> p6 "a special operators"
>
> p10 "for instance, [let f=e in e'] gets decomposed into \box[let f=e in e']" : f -> x
>
> p10 first rule for the transformation: two \vdash should be a left arrow and a squiggy right arrow, comma should be a dot
>
> p11 "are only tail calls in the DPS if ..." -> are tail calls in the DPS only if ...
>
> p15 "another alternatives"
>
> p17 "tail recursive [version"
>
> p18 font problem with "*base* falls back"
>
> p21 "some implementations that use the heap" : remove 'that' ?
