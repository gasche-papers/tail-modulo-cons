LATEXMK=latexmk -pdf -bibtex
.PHONY: mk
mk:
	$(MAKE) -C benchmarks
	$(LATEXMK)

clean:
	$(MAKE) clean -C benchmarks
	latexmk -c
	rm -f *.{log,bbl,nav,rev,snm,vrb,vtc}

.PHONY: all clean
