Dans l'implémentation du langage OCaml, les appels de fonctions
consomment la pile d'appel du système, dont la taille est fixée à une
limite très basse par défaut. Pour éviter les erreurs de dépassement
de pile (Stack Overflow), on doit programmer en prenant soin d'écrire
des fonctions récursives "terminales", qui ne consomment pas de pile
d'appel.

Cette discipline est une source de difficultés pour les débutants
comme pour les experts. Aux débutants il faut apprendre d'abord la
récursivité, et ensuite la récursivité terminale. Même les experts ne
sont pas d'accord sur la "bonne façon" d'écrire la fonction de base
List.map: la version directe et jolie n'est pas récursive-terminale,
elle est donc incorrecte sur de grandes entrées. La version terminale
la plus naturelle est (un peu) plus lente que la version
directe. Certaines bibliothèques proposent des horreurs de 300 lignes
implémentées en déroulant du code à la main, pour compenser cette
perte de performance.

Nous proposons une implémentation de "Tail Modulo Cons" (TMC) pour
OCaml. TMC est une transformation de programmes qui sait convertir
certaines définitions non-terminales en versions terminales, en
produisant du code en "passage par destination"
(destination-passing style). Contrairement à des approches génériques
comme le passage de continuations (CPS), TMC est moins expressive
(elle ne s'applique qu'aux exemples d'une forme bien spécifique) mais
produit du code plus efficace, équivalent en pratique à la version
directe. On peut enfin écrire List.map de façon jolie, correcte, et
rapide.

Dans notre article (en anglais), nous présenterons:
- l'usage de la transformation TMC
- les choix de conception que nous avons faits
  (transformation à demander explicitement, traitement des ambiguïtés)
- quelques questions restantes (portée et localité de la
  transformation, déduplication)
- une nouvelle présentation "modulaire" de la transformation TMC
- une étude de performances sur List.map. 
