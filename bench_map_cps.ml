let rec map f = function
| [] -> []
| x::xs -> 
  let y = f x in
  y :: map f xs

let rec map_cps f li k = match li with
| [] -> k []
| x :: xs ->
  let y = f x in
  map_cps f xs @@ fun ys -> k (y :: ys)

let map_from_cps f li = map_cps f li (fun x -> x)

let input = List.init 100_000 (fun i -> i)

let fun_to_test =
  match Sys.argv.(1) with
  | "direct" -> map
  | "cps" -> map_from_cps
  | _  | exception _ ->
      prerr_endline "First argument expected: 'direct' or 'cps.";
      exit 1

let n_iter =
  match int_of_string Sys.argv.(2) with
  | n -> n
  | exception _ ->
    prerr_endline "Second argument expected: the number of iterations";
    exit 1

let () =
  for _i = 1 to n_iter do
    ignore (fun_to_test Fun.id input)
  done
